import socket
from client import Client

if __name__ == '__main__':
    # get local machine host name
    ip = socket.gethostname()
    port = 5890
    buffer_size = 4096
    client = Client(ip, port, buffer_size)
    client.create_client()