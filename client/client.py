import sys
import socket
import select

class Client(object):

    __socket_list = []

    def __init__(self, ip, port, buffer_size):
        self.host = ip
        self.port = port
        self.buffer_size = buffer_size

    # This method identifies which client method is currently readable,
    # if the current client itself is readable then it receive data
    # else, client sends data to the server
    # Input Parameter   : client_socket->socket
    # Return Parameter  : N/A
    # Additional Details: N/A

    def manage_client(self, client_socket):
        try:
            while True:
                # identify which client is currently readable
                readable, writeable, exceptional = select.select(self.__socket_list, [], [])

                # if client is readable, then
                # client receive message from the server
                # else, client may send some data
                for sock in readable:
                    if sock == client_socket:
                        recv_msg = sock.recv(self.buffer_size).decode('utf-8')
                        # if message is empty, then
                        # server may disconnect the client
                        if not recv_msg:
                            print('Disconnected by the server!')
                            sys.exit()
                        data = recv_msg.split('~')
                        print(f'{data[0]}: {data[1]}')
                    else:
                        message = input()
                        # if message is not empty
                        if message:
                            # send the message to the server
                            client_socket.send(f'{message}'.encode('utf-8'))
                            # code for primary process
                            # message = input(f'[{username}]: ')
                            # if len(message)>0:
                            #     message_header = len(message)
                            #     client_socket.send(f'{message_header:4},{message}'.encode('utf-8'))
                            #
                            # while True:
                            #     data = client_socket.recv(4096).decode('utf-8')
                            #     #client is disconnected by server
                            #     if not data:
                            #         print('connection is closed by the server!')
                            #         sys.exit()
                            #     receive_message = data.split(',')
                            #     print(f'[{receive_message[0]}]: {receive_message[1]}')
        except KeyboardInterrupt:
            print('\nConnection closed successfully!')
            client_socket.close()
            sys.exit()

    # create a tcp client
    # Input Parameters : N/A
    # Return Parameters: N/A
    # Additional Details: enable using socket in non blocking mode and client side username validation
    def create_client(self):

        # create a socket for client
        # socket.AF_INET: address family ipv4
        # socket.SOCK_STREAM: connection type TCP
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # try to connect the server
        # if server is down,
        # fire up an exception
        try:
            client_socket.connect((self.host, self.port))
        except ConnectionRefusedError as e:
            print(f'Failed: {e}')
            sys.exit()

        # set client socket into non blocking mode
        client_socket.setblocking(False)

        # get username from the client
        username = input('username: ')

        # username client side validation,
        # check for empty username
        # close connection if username is empty
        if not username:
            print('Empty username is not acceptable!')
            client_socket.close()
            sys.exit()

        # send username to the server
        client_socket.send(f'{username}'.encode('utf-8'))

        # add standard i/o and recently created client connection to the list
        self.__socket_list.append(sys.stdin)
        self.__socket_list.append(client_socket)
        print("Let's start chat: \n")

        self.manage_client(client_socket)
