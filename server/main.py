import socket
from server import Server

if __name__ == '__main__':
    # return local machine hostname
    ip = socket.gethostname()
    port = 5890
    buffer_size = 4096
    queue = 10
    server = Server(ip, port, buffer_size, queue)
    server.server_setup()
