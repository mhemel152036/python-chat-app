import sys
import socket
import select
import json
import threading
import time
from datetime import datetime

class Server(object):
    __socket_list = []
    __json_dict = {}
    __clients = {}

    def __init__(self, ip, port, buffer_size, queue):
        self.host = ip
        self.port = port
        self.buffer_size = buffer_size
        self.queue = queue

    # check for json data
    # Input Parameter   : data->string
    # Return Parameter  : Boolean
    # Additional Details: N/A
    def is_json(self, data):
        try:
            # valid json data is convertable to dictionary object
            obj = json.loads(data)
            return True
        except ValueError as e:
            return False

    # store json data to the dictionary
    # Input Parameters  : data->string, client_socket->socket
    # Return Parameter  : N/A
    # Additional Details: Client socket is used as key, and data as value
    def store_json(self, client_socket, data):
        self.__json_dict[client_socket] = data

    # provide current system time
    # Input Parameter   : N/A
    # Return Parameter  : String
    # Additional Details: This method return time in H:M:S format
    def current_time(self):
        now = datetime.now().strftime('%H:%M:%S')
        return now

    # This method resume client connection
    # Input Parameter   : client_socket->socket
    # Return Parameter  : N/A
    # Additional Details: N/A
    def __resume_client(self, client_socket):
        # withdrawal blocked status
        self.__clients[client_socket]['block_status'] = False
        # inform client
        client_socket.send('SERVER[info]~ block resumed!'.encode('utf-8'))

    # This function inform the client if he send any torrent link
    # Input Parameter   : client_socket->socket
    # Return Parameter  : N/A
    # Additional Details: This method record sending history and block level
    def torrent_suspect(self, client_socket):
        # increase block level
        self.__clients[client_socket]['block_level'] = self.__clients[client_socket]['block_level'] + 1
        # get current system time
        send_time = self.current_time()
        # store sending time
        self.__clients[client_socket]['torrent_history'].append(send_time)
        # warn the client
        client_socket.send('server [warning]~Torrent link is not acceptable'.encode('utf-8'))


    # check if client is blockable or not
    # Input Parameter   : client_socket->socket
    # Return Parameter  : Boolean
    # Additional Details: This function convert duration in integer
    def is_blockable(self, client_socket):
        # fetch block level
        block_level = self.__clients[client_socket]['block_level']
        if block_level >= 3:
            # fetch torrent link sending history
            send_history = self.__clients[client_socket]['torrent_history']
            t0 = send_history[0]
            t1 = send_history[2]
            format = '%H:%M:%S'
            # compute interval time
            duration = datetime.strptime(t1, format) - datetime.strptime(t0, format)
            # take only minute in count
            duration = int(str(duration).split(':')[1])
            if duration>=0 and duration<=5:
                return True
            else:
                return False


    # check if username already exists or not
    # Input Parameters  : username->String
    # Return Parameters : Boolean
    # Additional Details: This method enable case insensitive username checking
    def valid_user(self, username):
        for client in self.__clients.values():
            if client['content'].lower() == username.lower():
                return False
        return True



    # Receive message content or user information from the client
    # Input Parameters  : client_socket->socket
    # Return Parameters : Object, Boolean
    # Additional Details: This function includes torrent link, json data identification
    def fetch_message(self, client_socket):
        content = client_socket.recv(self.buffer_size).decode('utf-8')

        # client close connection before delivering message
        if len(content) == 0:
            return False

        # check for json data
        if self.is_json(content):
            # store json data
            self.store_json(client_socket, content)
            return 'json'

        # check for torrent keyword inside message
        # check for block able client or not
        # if blockable then block the client
        if content.lower().find('torrent') >= 0:
            self.torrent_suspect(client_socket)
            if self.is_blockable(client_socket):
                return 'block'
            else:
                return 'warn'

        return {
            'content': content,
            'block_level': 0,
            'torrent_history': [],
            'block_status': False
        }

    # this method identifies which socket is currently ready to read
    # if readable socket is server socket itself then it receives a
    # message from the client else the server takes a new connection.
    # Input Parameters   : server_socket->socket
    # return Parameters  : N/A
    # Additional Details : mange socket in a non blocking mode
    def manage_server(self, server_socket):
        try:
            while True:
                # wait for the sockets ready to read
                # select takes three list
                    # ready to read
                    # ready to write
                    # exception list
                # select itself a blocking call so we set 0 to use it in non blocking mode
                readable, writeable, exceptional = select.select(self.__socket_list, [], [], 0)
                for sock in readable:
                    if sock is server_socket:
                        # accept a new client connection
                        client_socket, address = server_socket.accept()
                        new_client = self.fetch_message(client_socket)
                        # print('new client: ',new_client)

                        if not new_client:
                            # client_socket.send("server[error]~user name can't be empty!".encode('utf-8'))
                            # client_socket.close()
                            continue

                        if not self.valid_user(new_client['content']):
                            client_socket.send("SERVER[error]~client name already exists!".encode('utf-8'))
                            client_socket.close()
                            continue

                        # add new client connection
                        self.__socket_list.append(client_socket)
                        # keeps track of new client info
                        self.__clients[client_socket] = new_client

                        print(f'{new_client["content"]} : {address} joined our chat room...')
                    else:

                        # if user is blocked then
                        # skip client message
                        if self.__clients[sock]['block_status']:
                            message = self.fetch_message(sock)
                            continue

                        # client message
                        message = self.fetch_message(sock)
                        # if message is False,
                        # broken connection
                        if not message:
                            username = self.__clients[sock]['content']
                            print(f"{username} is in offline")
                            # socket exists
                            if sock in self.__socket_list:
                                # remove broken socket form the socket list
                                self.__socket_list.remove(sock)
                            # remove client info
                            del self.__clients[sock]
                            continue

                        if message is 'json':
                            continue

                        # if clint is blockable, then block the client and,
                        # add client after 10 minutes again
                        if message is 'block':
                            # if sock in socket_list:
                            # socket_list.remove(sock)
                            # sock.send('SERVER [notice]~ blocked by server for next 10 minits!'.encode('utf-8'))
                            # block_thread = threading.Timer(30.0, resume_client, (sock,))
                            # block_thread.start()
                            # block_thread.join()

                            # change block status to True
                            self.__clients[sock]['block_status'] = True
                            sock.send('SERVER [notice]~ blocked by server for next 10 minutes!'.encode('utf-8'))
                            # Resume method will be invoked after 1 minute
                            block_thread = threading.Timer(60.0, self.__resume_client, (sock,))
                            # start time count down
                            block_thread.start()
                            # print(clients[sock])
                            continue

                        # simply warn the client
                        elif message is 'warn':
                            print('Server got some torrent content!')
                            continue

                        # sender connection is ok and fetch the sender
                        sender = self.__clients[sock]
                        print(f'{sender["content"]}: sends a message')
                        for receiver_connection, receiver in self.__clients.items():
                            # sends message to all clients except the sender one
                            if receiver_connection != sock:
                                receiver_connection.send(f"{sender['content']}~{message['content']}".encode('utf-8'))
        except KeyboardInterrupt:
            print('\nServer down!')
            server_socket.close()
            sys.exit()


    # configure a tcp server socket
    # Input Parameters   : N/A
    # Return Parameters  : N/A
    # Additional Details : enable server socket to use in reuseable mode
    def server_setup(self):
        # socket.AF_INET: address family ipv4
        # socket.SOCK_STREAM: connection type TCP
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        # enable socket reusability
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            # bind socket to host's port number
            server_socket.bind((self.host, self.port))
        except Exception as e:
            print("Address already taken!")
            sys.exit()

        # define queue limit for the server
        server_socket.listen(self.queue)
        self.__socket_list.append(server_socket)
        print(f'server is running on {self.host}, {self.port}...')
        self.manage_server(server_socket)

